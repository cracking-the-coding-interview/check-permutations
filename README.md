1.2: Check Permutations
======================================

### STATUS:
* master: [![pipeline status](https://gitlab.com/cracking-the-coding-interview/check-permutations/badges/master/pipeline.svg)](https://gitlab.com/cracking-the-coding-interview/check-permutations/commits/master)
* develop: [![pipeline status](https://gitlab.com/cracking-the-coding-interview/check-permutations/badges/develop/pipeline.svg)](https://gitlab.com/cracking-the-coding-interview/check-permutations/commits/develop)

======================================
### Table of Contents
1. [Running Tests](https://gitlab.com/cracking-the-coding-interview/CheckPermutation#running-tests)
2. [Problem Statement](https://gitlab.com/cracking-the-coding-interview/CheckPermutation#problem-statement)
3. [Questions](https://gitlab.com/cracking-the-coding-interview/CheckPermutation#questions)
4. [Solution](https://gitlab.com/cracking-the-coding-interview/CheckPermutation#solution)
5. [Implementation](https://gitlab.com/cracking-the-coding-interview/CheckPermutation#implementation)
6. [Additional notes](https://gitlab.com/cracking-the-coding-interview/CheckPermutation#additional-notes)
7. [References](https://gitlab.com/cracking-the-coding-interview/CheckPermutation#references)

# Running Tests:
I'm using Gulp as a task runner, eslint
(with airbnb coding  standards) for code linting,
and mocha as a testing framework.  To run tests, after a fresh
clone of this repository, run:
```
npm install
gulp test
```
The grunt test task will run the lint and mocha tasks sequentially.  Those tasks will run eslint, and mocha.

# Problem Statement:
Given two strings, write a method to decide if one is a 
permutation of the other.

# Questions:

1. Is the empty string a permutation of an empty string?

2. How should I treat whitespace?  Should whitespace be removed
or treated as being part of the string being tested?

# Solution:

We need three functions, taking in two strings, and returning
a boolean.

We will need two helper functions, one to tell us if our
dicts are equal, and another to build our dicts from input
strings.  The third function, isPermutation, calls these
two helper functions to determine if the input strings
are permutations of one another.

If either of the two input strings are not strings, the
function should throw an Error.

A permutation of a string sounds like a rearrangement of the
characters in that string.  This sounds like an anagram, but
the permutation does not need to be made up of valid words.

I would start by checking the length of the input strings.
If the lengths are not equal, the strings cannot be
permutations of one another.  In this case, I return false.

Then I would construct two dicts, storing the
character, and the number of times that character appears
in each of the input strings.

Once these dicts were built, I would convert them
to sorted collections, and iterate through each one.  If
the strings are permutations of one another, the sorted
collections should be exactly the same length, contents, and order.

If the sorted collections are not the same length, we return
false.

If a key in one sorted collection doesn't exist in the other,
we return false.

If a key exists in both sorted collections, but have different
values in the sorted collections, we return false.

# Implementation:
I'll be using JavaScript, running in Node.  For testing, I will
be using Mocha, using the assert module for my assertions.

In my checkPermutation function, I'll be using objects with
key value pairs as a dict.  I'll use Object.keys() to pull
an array of keys from each dict, and call Array's .sort()
to sort each array of keys.  Each array of keys will have
a length parameter, and will be suitable for use as
sorted collections.

# Additional notes:

At first, I had expected this solution would only require one
function.  As I worked on the implementation, I realized
that this solution would be better if it were broken down
into three functions.

Testing was much easier, and simpler with this approach.

# References

