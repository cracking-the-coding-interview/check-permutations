const assert = require('assert');
const CheckPermutation = require('../src/check-permutation');

describe('isPermutation tests', () => {
  it('should return true if strings are empty strings', () => {
    const checkPermutation = new CheckPermutation();
    assert.equal(checkPermutation.isPermutation('', ''), true);
  });

  it('should return true when strings are permutations', () => {
    const checkPermutation = new CheckPermutation();
    assert.equal(checkPermutation.isPermutation('foo', 'oof'), true);
  });

  it('should return true when strings are permutations (Unicode characters, and whitespace)', () => {
    const checkPermutation = new CheckPermutation();
    assert.equal(checkPermutation.isPermutation('rab foo привет', 'привет oof bar'), true);
  });

  it('should return false when strings are not permutations (same length)', () => {
    const checkPermutation = new CheckPermutation();
    assert.equal(checkPermutation.isPermutation('foo', 'bar'), false);
  });

  it('should return false when strings are not permutations (different length)', () => {
    const checkPermutation = new CheckPermutation();
    assert.equal(checkPermutation.isPermutation('foo', 'toot'), false);
  });

  it('should return true when strings with whitespace are permutations', () => {
    const checkPermutation = new CheckPermutation();
    assert.equal(checkPermutation.isPermutation('foo bar', 'rab oof'), true);
  });

  it('should return false when strings are not permutations (white space is different)', () => {
    const checkPermutation = new CheckPermutation();
    assert.equal(checkPermutation.isPermutation('foo bar', 'rabxoof'), false);
  });

  it('should throw Error when inputs are not strings', () => {
    assert.throws(() => {
      const checkPermutation = new CheckPermutation();
      checkPermutation.isPermutation(0, 1);
    }, Error, 'Either one or both inputs are not strings!');
  });
});

describe('buildDict tests', () => {
  it('should build dict from simple string (no whitespace, no repeated characters', () => {
    const checkPermutation = new CheckPermutation();
    const input = 'fox';
    const expected = { f: 1, o: 1, x: 1 };
    const actual = checkPermutation.buildDict(input);

    assert.equal(checkPermutation.areDictsEqual(expected, actual), true);
  });

  it('should build dict from complex string (no whitespace, repeated characters', () => {
    const checkPermutation = new CheckPermutation();
    const input = 'foo';
    const expected = { f: 1, o: 2 };
    const actual = checkPermutation.buildDict(input);

    assert.equal(checkPermutation.areDictsEqual(expected, actual), true);
  });

  it('should build dict from complex string (whitespace, and repeated characters', () => {
    const checkPermutation = new CheckPermutation();
    const input = 'foo bar';
    const expected = {
      f: 1, o: 2, ' ': 1, b: 1, a: 1, r: 1,
    };
    const actual = checkPermutation.buildDict(input);

    assert.equal(checkPermutation.areDictsEqual(expected, actual), true);
  });

  it('should build dict from complex string (Unicode characters, whitespace, and repeated characters', () => {
    const checkPermutation = new CheckPermutation();
    const input = 'foo привет bar';
    const expected = {
      f: 1, o: 2, ' ': 2, п: 1, р: 1, и: 1, в: 1, е: 1, т: 1, b: 1, a: 1, r: 1,
    };
    const actual = checkPermutation.buildDict(input);

    assert.equal(checkPermutation.areDictsEqual(expected, actual), true);
  });

  it('should throw Error when input is not a string', () => {
    assert.throws(() => {
      const checkPermutation = new CheckPermutation();
      checkPermutation.buildDict(1);
    }, Error, 'input is not a string!');
  });
});

describe('areDictsEqual tests', () => {
  it('should return true when dicts are equal', () => {
    const checkPermutation = new CheckPermutation();
    const dict1 = { f: 1, o: 2 };
    const dict2 = { f: 1, o: 2 };

    assert.equal(checkPermutation.areDictsEqual(dict1, dict2), true);
  });

  it('should return true when dicts are equal (repeated values)', () => {
    const checkPermutation = new CheckPermutation();
    const dict1 = { f: 1, o: 2, x: 1 };
    const dict2 = { f: 1, o: 2, x: 1 };

    assert.equal(checkPermutation.areDictsEqual(dict1, dict2), true);
  });

  it('should return true when dicts are equal (whitespace keys)', () => {
    const checkPermutation = new CheckPermutation();
    const dict1 = {
      f: 1, o: 2, ' ': 1, b: 1, a: 1, r: 1,
    };
    const dict2 = {
      f: 1, o: 2, ' ': 1, b: 1, a: 1, r: 1,
    };

    assert.equal(checkPermutation.areDictsEqual(dict1, dict2), true);
  });

  it('should return true when dicts are equal (keyvals out of order)', () => {
    const checkPermutation = new CheckPermutation();
    const dict1 = { f: 1, o: 2, x: 1 };
    const dict2 = { f: 1, x: 1, o: 2 };

    assert.equal(checkPermutation.areDictsEqual(dict1, dict2), true);
  });

  it('should return false when dicts are not equal (different keyvals, same keys, different values)', () => {
    const checkPermutation = new CheckPermutation();
    const dict1 = { f: 1, o: 2 };
    const dict2 = { f: 1, o: 3 };

    assert.equal(checkPermutation.areDictsEqual(dict1, dict2), false);
  });

  it('should return false when dicts are not equal (different keyvals, different keys)', () => {
    const checkPermutation = new CheckPermutation();
    const dict1 = { f: 1, x: 2 };
    const dict2 = { f: 1, o: 2 };

    assert.equal(checkPermutation.areDictsEqual(dict1, dict2), false);
  });

  it('should return false when dicts are not equal (different size dicts)', () => {
    const checkPermutation = new CheckPermutation();
    const dict1 = { f: 1, o: 2 };
    const dict2 = { f: 1, o: 3, x: 1 };

    assert.equal(checkPermutation.areDictsEqual(dict1, dict2), false);
  });

  it('should throw Error when inputs are not dicts', () => {
    assert.throws(() => {
      const checkPermutation = new CheckPermutation();
      checkPermutation.areDictsEqual(1, 2);
    }, Error, 'inputs are not dicts!');
  });
});
